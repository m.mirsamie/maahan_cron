<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require("class/DB.php");

class Flight
{
    private $_db;
    
    function __construct() {
        $this->_db=new DB();
    }

    public function addFlight($flight){
        return $this->_db->insert("flight", $flight);
    }
}