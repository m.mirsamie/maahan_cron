<?php
error_reporting(0);
class addParvaz{
    public $m;
    public function __construct() {
        $this->m = new mysqli('localhost','root','3068145','mahan');
    }
    public function add($flight){
        $ok = FALSE; 
        if($this->m->connect_errno!==FALSE)
        {
            $ok = $this->m->query("insert into `parvaz` (`FlightNumber`, `ArrivalDateTime`, `DepartureDateTime`, `JourneyDuration`, `RPH`, `DestinationLocation_text`, `DestinationLocation_iata`, `OriginLocation_text`, `OriginLocation_iata`, `zarfiat`) values ('".$flight['FlightNumber']."', '".$flight['ArrivalDateTime']."', '".$flight['DepartureDateTime']."', '".$flight['JourneyDuration']."', '".$flight['RPH']."', '".$flight['DestinationLocation']['text']."', '".$flight['DestinationLocation']['iata']."', '".$flight['OriginLocation']['text']."', '".$flight['OriginLocation']['iata']."', 9)");
        }
        return($ok);
    }
}
include_once 'class/conf.php';
include_once 'class/MahanWebService.php';
include_once 'class/Flight.php';
$mah = new MahanWebService();
$flightClass=new Flight();
$db=new DB();
$adl = 5;
$chd = 0;
$inf = 0;
//    $froms=$db->select("mahan_city", array());
//    $tos=$db->select("mahan_city", array());
$froms=array('IKA', 'KBL', 'MHD', 'THR', 'SYZ', 'ISF');
$tos=array('IKA', 'KBL', 'MHD', 'THR', 'SYZ', 'ISF');
$today=date("Y-m-d");
$startTime=time();
foreach ($froms as $from){
    foreach ($tos as $to){
        if($from==$to) continue;
        for($i=0; $i<15; $i++){
            $DepDate=date("Y-m-d", strtotime($today.' + '.$i.' days'));
            $DepDate.="T00:00:00";
            $flights = $mah->getAvailability($from, $to, $DepDate, $adl, $chd, $inf);
            if(count($flights['errors'])>0)
            {
                $errMessage=array(
                    'Invalid value [Invalid From and/or To Airport(s)]'=>'در حال حاضر پروازی در این خط وجود ندارد.',
                    'No availability [No matching flight found]'=>'ظرفیت موجود نیست.',
                );
                $message=($errMessage[$flights['errors'][0]['Msg']])?$errMessage[$flights['errors'][0]['Msg']]:"عملیات با خطا مواجه شد.";
                echo "from $from to $to in $DepDate : $message<br/>\n";
            }
            else
            {
                $results=$flights['flights'];
                $class_gheymats=$flights['class_gheymat'];
                foreach ($results as $result){
                    foreach ($class_gheymats as $class_gheymat){
                        $result[agency_id]=0;
                        $result[from_city]=$from;
                        $result[to_city]=$to;
                        $result[flight_id]=0;
                        $depTime=$result[DepartureDateTime];
                        $depTime=explode("T", $depTime);
                        $result[fdate]=$depTime[0];
                        $result[ftime]=$depTime[1];
                        $result[ltime]=$result[JourneyDuration];
                        $result[capacity]=5;
                        $result[class_ghimat]=$class_gheymat;
                        $result['class']=1;
                        $result['typ']=1;
                        $result['en']=3;
                        $result['agency_name']='Mahan';
                        $result['agency_site']='http://www.mahan.aero/';
                        $result['buy_time']=0;
                        $result['source_id']=4;
                        $flightClass->addFlight($result);
                    }
                }
            }
        }
    }
}
$finishTime=time();
$bench=$finishTime-$startTime;
echo "<p>زمان اجرای درخواست : $bench ثانیه</p>";
